import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FeedReaderComponent} from './feed-reader/feed-reader.component';

const routes: Routes = [
  {path: "", component: FeedReaderComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
