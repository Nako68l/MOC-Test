import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'beautifulUrl'
})
export class BeautifulUrlPipe implements PipeTransform {

  transform(link: string): string {
    let linkArray = link.split('/');
    let linkDomain = linkArray[2];
    return linkDomain.replace("www.", "");
  }

}
