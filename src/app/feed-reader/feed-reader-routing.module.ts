import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FeedReaderComponent} from './feed-reader.component';

const routes: Routes = [
  {path: "", component: FeedReaderComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedReaderRoutingModule { }
