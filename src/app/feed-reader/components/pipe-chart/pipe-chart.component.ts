import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'pipe-chart',
  templateUrl: './pipe-chart.component.html',
  styleUrls: ['./pipe-chart.component.css']
})
export class PipeChartComponent implements OnInit{
  @ViewChild('canvas') public canvas: ElementRef;
  @ViewChild('legend') public legendElement: ElementRef;

  @Input() public width = 400;
  @Input() public height = 400;
  @Input() data;

  private ctx: CanvasRenderingContext2D;
  legend: string = '';

  ngOnInit(): void {
    const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
    this.ctx = canvasEl.getContext('2d');
    canvasEl.width = this.width;
    canvasEl.height = this.height;
    this.draw(this.data)
  }

  draw(data: Map<string, { value, color }>) {
    let totalValue = 0;
    data.forEach(obj => totalValue += obj.value);

    let startAngle = 0;
    data.forEach((obj,key) => {
      let sliceAngle = 2 * Math.PI * obj.value / totalValue;
      this.drawPieSlice(
        this.ctx,
        this.width / 2,
        this.height / 2,
        Math.min(this.width / 2, this.height / 2),
        startAngle,
        startAngle + sliceAngle,
        obj.color
      );
      this.legend+=`<div><span style='display:inline-block;width:20px;background-color:${obj.color}'>&nbsp;</span>\t<b>${key}</b>\t ${((obj.value/totalValue)*100).toFixed(2)}%</div>`;
      startAngle += sliceAngle;
    });
    this.legendElement.nativeElement.innerHTML = this.legend;
  };

  drawPieSlice(ctx, centerX, centerY, radius, startAngle, endAngle, color) {
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(centerX, centerY);
    ctx.arc(centerX, centerY, radius, startAngle, endAngle);
    ctx.closePath();
    ctx.fill();
  }
}
