import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FeedListService} from '../../services/feed-list.service';

@Component({
  selector: 'feed-list',
  templateUrl: './feed-list.component.html',
  styleUrls: ['./feed-list.component.css']
})
export class FeedListComponent implements OnInit {
  feedLinks: string[];
  newFeedLink: string;
  selectedLink: string;
  @Output() onSelectedLink = new EventEmitter<string>();

  constructor(private feedListService: FeedListService) { }

  ngOnInit() {
    this.feedLinks = this.feedListService.getFeedLinks();
    this.selectedLink = this.feedListService.getSelectedLink();
    this.onSelectedLink.emit(this.selectedLink);
  }

  saveFeedLink(){
    if(this.urlNotEmpty()){
      this.feedListService.saveFeedLink(this.newFeedLink);
      this.feedLinks = this.feedListService.getFeedLinks();
      this.newFeedLink = '';
    }
  }

  urlNotEmpty() {
    return this.newFeedLink && this.newFeedLink.trim().match("https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.,!~#?&//=]*)")
  }

  deleteFeedLink(link) {
    this.feedListService.deleteFeedLink(link);
    this.selectedLink = "";
    this.onSelectedLink.emit(this.selectedLink);
    this.feedLinks = this.feedListService.getFeedLinks();
  }

  selectFeedLink(feedLink: string){
    this.selectedLink = feedLink;
    this.onSelectedLink.emit(this.selectedLink);
    this.feedListService.setSelectedLink(feedLink);
  }
}
