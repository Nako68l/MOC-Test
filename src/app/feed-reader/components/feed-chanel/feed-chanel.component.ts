import {Component, Inject, Input, OnInit, SimpleChanges} from '@angular/core';
import {FeedInfo} from '../../models/feed-info';
import {FeedService} from '../../services/feed.service';
import {FeedItem} from '../../models/feed-item';
import {CharacterStatisticService} from '../../services/character-statistic.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {StatisticsDialogComponent} from '../statistics-dialog/statistics-dialog.component';

@Component({
  selector: 'feed-chanel',
  templateUrl: './feed-chanel.component.html',
  styleUrls: ['./feed-chanel.component.css']
})
export class FeedChanelComponent implements OnInit {
  @Input() selectedLink: string;
  chanelFeed: FeedInfo;
  feedError: Error;

  ngOnChanges(changes: SimpleChanges) {
    this.feedService.getFeedContent(this.selectedLink).subscribe(
      data => this.chanelFeed = {...data},
      error => this.feedError = error
    );
  }

  constructor(public dialog: MatDialog, private feedService: FeedService, private characterStatistic: CharacterStatisticService) {
  }

  ngOnInit() {
    this.feedService.getFeedContent(this.selectedLink).subscribe(
      data => this.chanelFeed = {...data},
      error => this.feedError = error
    );
  }
  getStatistic(feedItem: FeedItem) {
    this.dialog.open(StatisticsDialogComponent, {
      width: '600px',
      data:  this.characterStatistic.getTextStatistic(feedItem.title + feedItem.description)
  });
  }
}


