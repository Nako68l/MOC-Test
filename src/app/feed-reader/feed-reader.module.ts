import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FeedReaderRoutingModule} from './feed-reader-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {FeedService} from './services/feed.service';
import {FeedListComponent} from './components/feed-list/feed-list.component';
import {SharedMaterialModule} from '../shared-material/shared-material.module';
import {FormsModule} from '@angular/forms';
import {FeedReaderComponent} from './feed-reader.component';
import {FeedListService} from './services/feed-list.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BeautifulUrlPipe} from './pipes/beautiful-url.pipe';
import {FeedChanelComponent} from './components/feed-chanel/feed-chanel.component';
import {CharacterStatisticService} from './services/character-statistic.service';
import {PipeChartComponent} from './components/pipe-chart/pipe-chart.component';
import { StatisticsDialogComponent } from './components/statistics-dialog/statistics-dialog.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    HttpClientModule,
    SharedMaterialModule,
    BrowserAnimationsModule,
    FeedReaderRoutingModule,
  ],
  declarations: [
    FeedListComponent,
    FeedReaderComponent,
    BeautifulUrlPipe,
    FeedChanelComponent,
    PipeChartComponent,
    StatisticsDialogComponent
  ],
  entryComponents: [
    StatisticsDialogComponent,
  ],
  providers: [
    FeedService,
    FeedListService,
    CharacterStatisticService,
  ]

})
export class FeedReaderModule {
}
