export interface FeedItem {
  title: string;
  pubDate: string;
  link: string;
  guild: string;
  author: string;
  thumbnail: string;
  description: string;
  content: string;
  enclosure: {
    link: string;
    type: string;
    length: number;
  };
  categories: string[];
}
