import {FeedItem} from './feed-item';

export interface FeedInfo {
  status: string;
  feed: {
    url: string;
    title: string;
    description: string;
    author: string;
    link: string;
    img: string;
  };
  items: FeedItem[]
}
