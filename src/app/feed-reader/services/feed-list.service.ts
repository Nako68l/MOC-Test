import {Injectable} from '@angular/core';

@Injectable()
export class FeedListService {
  private readonly linksSeparator = ' | ';
  private readonly feedLinksKey = 'feed_links';

  constructor() {
  }

  saveFeedLink(newFeedLink: string): void {
    let existingFeedLinks = localStorage.getItem(this.feedLinksKey);
    if (!existingFeedLinks) existingFeedLinks = '';
    localStorage.setItem(this.feedLinksKey, existingFeedLinks + newFeedLink + this.linksSeparator);
  }

  getFeedLinks(): string[] {
    let feedLinks = localStorage.getItem(this.feedLinksKey);
    if (feedLinks) {
      return feedLinks.slice(0, -3).split(this.linksSeparator);
    }
    return [];
  }

  deleteFeedLink(link: string) {
    let feedLinks = localStorage.getItem(this.feedLinksKey);
    let updatedFeedLinks = feedLinks.replace(link + this.linksSeparator, '');
    localStorage.setItem(this.feedLinksKey, updatedFeedLinks);
  }

  getSelectedLink() {
    return localStorage.getItem('selected_link')
  }

  setSelectedLink(link: string) {
    localStorage.setItem('selected_link', link);
  }
}
