import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FeedInfo} from '../models/feed-info';

@Injectable()
export class FeedService {

    private rssToJsonServiceBaseUrl: string = 'https://rss2json.com/api.json?rss_url=';

    constructor(private http: HttpClient) { }

    getFeedContent(url: string) {
        return this.http.get<FeedInfo>(this.rssToJsonServiceBaseUrl + url)
    }
}
